package demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {

  @Test
  public void testAdd() {
    Calculator calculator = new Calculator();
    int result = calculator.add(3, 7);
    assertEquals(10, result); // Check if the result is equal to 10
  }

  @Test
  public void testSubtract() {
    Calculator calculator = new Calculator();
    int result = calculator.subtract(10, 4);
    assertEquals(6, result); // Check if the result is equal to 6
  }
}
