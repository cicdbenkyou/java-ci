package demo;

import school.Student;
import com.google.gson.Gson;
import com.google.common.base.Joiner;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student a = new Student();
		System.out.printf("test maven build 5");
		System.out.println(a.getA()+a.getB());
		Gson gson = new Gson();
		Student student = new Student(); 
		String json = gson.toJson(student);
		System.out.println(json);
		Joiner joiner = Joiner.on("_ ");
		String result = joiner.join("apple", "banana", "cherry");
		System.out.println(result); // Kết quả: "apple_ banana_ cherry"

	}

}
